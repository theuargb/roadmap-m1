<?php

class E4_E4T1_IndexController extends Mage_Core_Controller_Front_Action
{
    private function _getAllCountdownDates(int $productId): array
    {
        $currentProduct = Mage::getModel('catalog/product')->load($productId);
        if (!$currentProduct) {
            return [];
        }

        $today = strtotime(date('Y-m-d', time()) . '00:00:00');
        $dates = [];

        $specialPriceDate = strtotime($currentProduct->getSpecialToDate());
        if ($specialPriceDate >= $today) {
            $dates[] = $specialPriceDate;
        }

        $catalogRules = Mage::getModel('catalogrule/rule')->getCollection()->addIsActiveFilter(1);
        foreach ($catalogRules as $catalogRule) {
            $affectedProductIds = $catalogRule->getMatchingProductIds();
            if (array_key_exists($currentProduct->getId(), $affectedProductIds)) { // ignoring site_id
                $catalogRuleDate = strtotime($catalogRule->getToDate());
                if ($catalogRuleDate >= $today) {
                    $dates[] = $catalogRuleDate;
                }
            }
        }

        return $dates;
    }

    private function _getProductIdFromRequest(): int
    {
        return $this->getRequest()->getParams()['productId'] ?? -1;
    }

    public function getCountdownDateAction()
    {
        $priceToDates = $this->_getAllCountdownDates($this->_getProductIdFromRequest());
        echo min($priceToDates);
    }
}