<?php

class E4_E4T1_Block_Countdown extends Mage_Core_Block_Template
{

    public function getCurrentProductId(): int
    {
        return Mage::registry('current_product')->getId();
    }

}