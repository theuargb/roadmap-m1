<?php

class E2_E2T2_Block_Index extends Mage_Core_Block_Template
{

    public function getSelectedCategoryId(): int
    {
        $categoryId = $parameters = $this->getRequest()->getParams()['categoryId'];
        if (!$categoryId) return -1;

        return $categoryId;
    }

    public function getCategories(): array
    {
        return Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('id')
            ->addAttributeToSelect('name')
            ->load()
            ->toArray();
    }

    public function getProductsInCategory(): array
    {
        $categoryId = $this->getSelectedCategoryId();
        if ($categoryId == -1) return [];

        return Mage::getModel('catalog/category')->load($categoryId)
            ->getProductCollection()
            ->addAttributeToFilter('status', ['eq' => 1])
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->load()
            ->toArray();
    }

}
