<?php

class E2_E2T6_Block_Index extends Mage_Core_Block_Template
{

    public function getSelectedCatalogPriceRule()
    {
        $productId = $parameters = $this->getRequest()->getParams()['productId'];
        if (!$productId) return null;

        return Mage::getModel('catalogrule/rule')->load($productId);
    }

    public function getCatalogPriceRules(): array
    {
        $rules = Mage::getModel('catalogrule/rule')->getCollection();

        $result = [];
        foreach ($rules as $rule) {
            $result[] = Mage::getModel('catalogrule/rule')->load($rule->getId());
        }

        return $result;
    }

    public function getProductNameById(int $productId)
    {
        return Mage::getModel('catalog/product')->load($productId)->getName();
    }

}