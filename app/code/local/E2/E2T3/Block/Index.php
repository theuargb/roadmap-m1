<?php

class E2_E2T3_Block_Index extends Mage_Core_Block_Template
{

    public function getGroupedProducts(): array
    {
        return Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToFilter('type_id', ['eq' => 'grouped'])
            ->addAttributeToSelect('sku')
            ->load()
            ->toArray();
    }

}