<?php

class E2_E2T5_Block_Index extends Mage_Core_Block_Template
{

    public function getSelectedProduct()
    {
        $productId = $parameters = $this->getRequest()->getParams()['productId'];
        if (!$productId) return null;
        return Mage::getModel('catalog/product')->load($productId);
    }

    public function getSelectedProductCatalogRules(): ?array
    {
        $selectedProduct = $this->getSelectedProduct();
        if (!$selectedProduct) return null;

        $catalogRules = Mage::getModel('catalogrule/rule')->getCollection()->addIsActiveFilter(1);

        $result = [];
        foreach ($catalogRules as $catalogRule) {
            $affectedProductIds = $catalogRule->getMatchingProductIds();
            if (array_key_exists($selectedProduct->getId(), $affectedProductIds)) { // ignoring site_id
                $result[] = $catalogRule;
            }
        }

        return $result;
    }

    public function getProducts(): array
    {
        return Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('id')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->load()
            ->toArray();
    }

}