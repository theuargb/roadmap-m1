<?php

class E2_E2T1_Block_Index extends Mage_Core_Block_Template
{

    public function getDatesRangeFromRequestParameters(): array
    {
        $parameters = $this->getRequest()->getParams();

        $from = $parameters['from'] ?? date('yy-m-d');
        $to = $parameters['to'] ?? date('yy-m-d');

        return [$from, $to];
    }

    public function getSoldProducts(): array
    {
        list($from, $to) = $this->getDatesRangeFromRequestParameters();
        $orders = Mage::getModel('sales/order')->getCollection()
            ->addAttributeToFilter('created_at', ['from' => $from, 'to' => $to])
            ->addAttributeToFilter('status', ['eq' => Mage_Sales_Model_Order::STATE_COMPLETE]);

        $result = [];
        foreach ($orders as $order) {
            foreach ($order->getAllItems() as $product) {
                $result[] = $product;
            }
        }

        return $result;
    }

}