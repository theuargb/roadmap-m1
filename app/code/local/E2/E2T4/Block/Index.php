<?php

class E2_E2T4_Block_Index extends Mage_Core_Block_Template
{

    public function getSelectedProduct()
    {
        $productId = $parameters = $this->getRequest()->getParams()['productId'];
        if (!$productId) return null;

        return Mage::getModel('catalog/product')->load($productId);
    }

    public function getProducts(): array
    {
        return Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('id')
            ->addAttributeToSelect('sku')
            ->addAttributeToSelect('name')
            ->load()
            ->toArray();
    }


}