<?php

class E3_E3T1_Model_System_Config_Source_Category
{

    function getCategoriesTreeView(): array
    {
        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToSort('path', 'asc')
            ->addFieldToFilter('is_active', ['eq' => '1'])
            ->load()
            ->toArray();

        $categoryList = [];
        foreach ($categories as $catId => $category) {
            if (isset($category['name'])) {
                $categoryList[] = [
                    'label' => $category['name'],
                    'level' => $category['level'],
                    'value' => $catId
                ];
            }
        }
        return $categoryList;
    }

    public function toOptionArray(): array
    {
        $options = [];

        $options[] = [
            'label' => Mage::helper('E3T1')->__('-- None --'),
            'value' => ''
        ];

        $categoriesTreeView = $this->getCategoriesTreeView();

        foreach ($categoriesTreeView as $value) {
            $catName = $value['label'];
            $catId = $value['value'];
            $catLevel = $value['level'];

            $hyphen = '-';
            for ($i = 1; $i < $catLevel; $i++) {
                $hyphen = $hyphen . "-";
            }

            $catName = $hyphen . ' ' . $catName;

            $options[] = [
                'label' => $catName,
                'value' => $catId
            ];
        }

        return $options;
    }

}