<?php


class E3_E3T1_Block_ProductTechString extends Mage_Core_Block_Template
{
    public function getCurrentProductCategoryId($currentProduct): int
    {
        return array_shift($currentProduct->getCategoryCollection()->load()->toArray())['entity_id'];
    }

    public function isCurrentProductApplicableForTechString(): bool
    {
        $currentProduct = Mage::registry('current_product');
        $applicableCategories = Mage::getStoreConfig('catalog/pts/applicableCategories');
        $applicableCategories = explode(',', $applicableCategories);
        return in_array($this->getCurrentProductCategoryId($currentProduct), $applicableCategories);
    }

    public function getTechStringForCurrentProduct(): string
    {
        $currentProduct = Mage::registry('current_product');
        $categoryId = $this->getCurrentProductCategoryId($currentProduct);
        return "{$categoryId}_{$currentProduct->getId()}_{$currentProduct->getSku()}_{$currentProduct->getTypeId()}";
    }
}